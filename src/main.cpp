#include <io.hpp>
#include <example.hpp>

#include <iostream>
#include <string>

#include <measures.hpp>

int main(int argc, char *argv[])
{

    std::cout << "Hello !" << std::endl;

    if (argc < 2)
    {
        throw std::invalid_argument("This program expects at least 1 argument (path to a mesh).");
    }

    const std::string meshPath = std::string{argv[1]};

    geomAlgoLib::Polyhedron myMesh;

    geomAlgoLib::readOFF(meshPath, myMesh);

    auto genus = geomAlgoLib::computeGenus(myMesh);
    std::cout << "The Genus of [" << meshPath << "] is = " << std::to_string(genus) << std::endl;

    geomAlgoLib::Facet_double_map map_levels_perimetre = geomAlgoLib::mesurePerimetre(myMesh);

    geomAlgoLib::Facet_double_map map_levels_area = geomAlgoLib::mesure_air(myMesh);

    // Create a colored version
    geomAlgoLib::Color perimetre_color1 = geomAlgoLib::Color(0, 0, 0.4);
    geomAlgoLib::Color perimetre_color2 = geomAlgoLib::Color(0, 0.4, 0);
    geomAlgoLib::Color area_color1 = geomAlgoLib::Color(0.4, 0, 0.4);
    geomAlgoLib::Color area_color2 = geomAlgoLib::Color(0.6, 0, 0);

    // create labels
    geomAlgoLib::Label label_perimtre = geomAlgoLib::segmentation(
        map_levels_perimetre,
        0.09,
        "Big perimetre",
        "small perimetre",
        perimetre_color1,
        perimetre_color2);

    geomAlgoLib::Label label_area = geomAlgoLib::segmentation(
        map_levels_area,
        0.0007,
        "Big area",
        "small area",
        area_color1,
        area_color2);

    // create vector
    std::vector<geomAlgoLib::Label> labels;
    labels.push_back(label_perimtre);
    labels.push_back(label_area);
    //  coloring

    // geomAlgoLib::file_coloring(myMesh, map_levels_area, 0.000841001, "../data/file.off");
    geomAlgoLib::file_coloring(myMesh, labels, "../data/file.off");
    //  geomAlgoLib::writeOFF(myMesh, "output.off");

    std::cout << "The end..." << std::endl;
    return 0;
}