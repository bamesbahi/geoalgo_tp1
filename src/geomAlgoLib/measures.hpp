#pragma once

#include "types.hpp"

namespace geomAlgoLib
{
    struct Color
    {
        double r, g, b;
        Color(double red, double green, double blue) : r(red), g(green), b(blue) {}
    };
    struct Label
    {
        Color firstColor, secondColor;
        std::string etiquette1, etiquette2;
        Facet_string_map map;
        Label(Color firstColor, Color secondColor,
              std::string etiquette1, std::string etiquette2,
              Facet_string_map map)
            : firstColor(firstColor), secondColor(secondColor), etiquette1(etiquette1), etiquette2(etiquette2), map(map) {}
    };

    Color addColors(const Color &a, const Color &b);
    Facet_double_map mesurePerimetre(Polyhedron &mesh);
    Facet_double_map mesure_air(Polyhedron &mesh);
    void file_coloring(const Polyhedron &mesh, Facet_double_map &facet_mesure, double seuil, const std::string &name);
    void file_coloring(const Polyhedron &mesh, Label &label, const std::string &file_path);
    void file_coloring(const Polyhedron &mesh, const std::vector<Label> &labels, const std::string &file_path);
    Label segmentation(
        Facet_double_map &measure_map,
        double seuil,
        std::string first_string,
        std::string second_string,
        Color first_color,
        Color second_color);

}