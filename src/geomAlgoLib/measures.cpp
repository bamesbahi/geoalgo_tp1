#include <math.h>
#include <iterator>
#include "measures.hpp"

#include "types.hpp"

#include <iostream>
#include <fstream>

namespace geomAlgoLib
{
    Color addColors(const Color &a, const Color &b)
    {
        return Color((a.r + b.r > 1) ? 1 : (a.r + b.r), // Limit the sum to 1 if it exceeds 1
                     (a.g + b.g > 1) ? 1 : (a.g + b.g),
                     (a.b + b.b > 1) ? 1 : (a.b + b.b));
    }

    Facet_double_map mesurePerimetre(Polyhedron &mesh)
    {
        double perimetre_total = 0;
        int number_of_verticies = 0;
        Facet_iterator it = mesh.facets_begin();
        Facet_double_map perimetre_par_face;
        std::cout << "Mesuring Facet's Perimetre" << std::endl;
        while (it != mesh.facets_end())
        {
            Halfedge_facet_circulator j = it->facet_begin();
            double distance = 0;
            do
            {
                distance += std::sqrt(CGAL::squared_distance(j->vertex()->point(), (j++)->vertex()->point()));

            } while ((j != it->facet_begin()));

            std::cout << distance << std::endl;
            perimetre_par_face[it] = distance;
            perimetre_total += distance;
            it++;
        }
        return perimetre_par_face;
    }
    Facet_double_map mesure_air(Polyhedron &mesh)
    {
        Facet_iterator it = mesh.facets_begin();
        Facet_double_map air_par_face;
        double air_tmp = 0;
        std::cout << "Measuring Facet's Areas" << std::endl;
        while (it != mesh.facets_end())
        {
            Halfedge_facet_circulator j = it->facet_begin();

            air_tmp = 0;
            do
            {
                CGAL::Point_3 p1 = j->vertex()->point();
                CGAL::Point_3 p2 = (j->next())->vertex()->point();
                CGAL::Point_3 p3 = (j->next()->next())->vertex()->point();

                air_tmp += std::sqrt(CGAL::squared_area(p1, p2, p3));
                if (j->next() != it->facet_begin())
                {
                    j++;
                }

            } while ((++j) != it->facet_begin());
            air_par_face[it] = air_tmp;
            std::cout << air_tmp << std::endl;
            it++;
        }
        return air_par_face;
    }
    // map de mesures -> coloring
    void file_coloring(const Polyhedron &mesh, Facet_double_map &facet_mesure, double seuil, const std::string &file_path)
    {

        std::cout << "coloring mesh" << std::endl;

        std::fstream file(file_path, std::ios::in | std::ios::out);

        if (!file.is_open())
        {
            std::cerr << "Error: Failed to open the file\n";
        }
        CGAL::set_ascii_mode(file);
        file.seekg(0);

        file << "COFF"
             << std::endl;
        file << mesh.size_of_vertices() << ' '
             << mesh.size_of_facets() << " 0" << std::endl;

        std::copy(mesh.points_begin(), mesh.points_end(),
                  std::ostream_iterator<Kernel::Point_3>(file, "\n"));

        for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
        {
            Halfedge_facet_circulator j = i->facet_begin();

            CGAL_assertion(CGAL::circulator_size(j) >= 3);

            file << CGAL::circulator_size(j) << ' ';
            do
            {
                file << ' ' << std::distance(mesh.vertices_begin(), j->vertex());
                j++;

            } while (j != i->facet_begin());
            if (facet_mesure[i] > seuil)
            {
                file << ' ' << "0.0 0.0 1.0";
            }
            else
            {
                file << ' ' << "0.0 1.0 0.0";
            }

            file << std::endl;
        }

        file.close();
    }
    Label segmentation(
        Facet_double_map &measure_map,
        double seuil,
        std::string first_string,
        std::string second_string,
        Color first_color,
        Color second_color)
    {
        Facet_string_map segmentation_map;
        auto iter = measure_map.begin();
        for (; iter != measure_map.end(); ++iter)
        {
            if (iter->second > seuil)
            {
                segmentation_map[iter->first] = first_string;
            }
            else
            {
                segmentation_map[iter->first] = second_string;
            }
        }
        return Label(first_color, second_color, first_string, second_string, segmentation_map);
    }
    void file_coloring(const Polyhedron &mesh, Label &label, const std::string &file_path)
    {
        std::cout << "coloring mesh" << std::endl;

        std::fstream file(file_path, std::ios::in | std::ios::out);

        if (!file.is_open())
        {
            std::cerr << "Error: Failed to open the file\n";
        }
        CGAL::set_ascii_mode(file);
        file.seekg(0);

        file << "COFF"
             << std::endl;
        file << mesh.size_of_vertices() << ' '
             << mesh.size_of_facets() << " 0" << std::endl;

        std::copy(mesh.points_begin(), mesh.points_end(),
                  std::ostream_iterator<Kernel::Point_3>(file, "\n"));

        for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
        {
            Halfedge_facet_circulator j = i->facet_begin();

            CGAL_assertion(CGAL::circulator_size(j) >= 3);

            file << CGAL::circulator_size(j) << ' ';
            do
            {
                file << ' ' << std::distance(mesh.vertices_begin(), j->vertex());
                j++;

            } while (j != i->facet_begin());
            if (label.map[i] == label.etiquette1)
            {
                // ajout de couleur
                file << ' ' << label.firstColor.r << ' ' << label.firstColor.g << ' ' << label.firstColor.b;
            }
            else
            {
                // ajout de couleur
                file << ' ' << label.secondColor.r << ' ' << label.secondColor.g << ' ' << label.secondColor.b;
            }

            file << std::endl;
        }
        file.close();
    }
    // coloration à base de plusieur label
    void file_coloring(const Polyhedron &mesh, const std::vector<Label> &labels, const std::string &file_path)
    {
        std::cout << "coloring mesh" << std::endl;

        std::fstream file(file_path, std::ios::in | std::ios::out);

        if (!file.is_open())
        {
            std::cerr << "Error: Failed to open the file\n";
        }
        CGAL::set_ascii_mode(file);
        file.seekg(0);

        file << "COFF"
             << std::endl;
        file << mesh.size_of_vertices() << ' '
             << mesh.size_of_facets() << " 0" << std::endl;

        std::copy(mesh.points_begin(), mesh.points_end(),
                  std::ostream_iterator<Kernel::Point_3>(file, "\n"));

        for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
        {
            Halfedge_facet_circulator j = i->facet_begin();

            CGAL_assertion(CGAL::circulator_size(j) >= 3);

            file << CGAL::circulator_size(j) << ' ';
            do
            {
                file << ' ' << std::distance(mesh.vertices_begin(), j->vertex());
                j++;

            } while (j != i->facet_begin());
            Color r(0, 0, 0);
            for (const auto &label : labels)
            {
                if (label.map.find(i) != label.map.end()) // Vérification de l'existance de l'étiquette
                {
                    if (label.map.at(i) == label.etiquette1)
                    {
                        r = addColors(r, label.firstColor);
                    }
                    else
                    {
                        r = addColors(r, label.secondColor);
                    }
                }
            }

            file << ' ' << r.r << ' ' << r.g << ' ' << r.b;

            file << std::endl;
        }
        file.close();
    }
}